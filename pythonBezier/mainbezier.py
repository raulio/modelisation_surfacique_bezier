import numpy as np

from bezierPatchEval import bezierPatchEval
from plotBezierSurface3D import plotSurface, plotControPoly


def loadControlPoints(filename):
    points = np.loadtxt(filename)
    return points


bezierSurf = loadControlPoints("surface4.txt")

nb = len(bezierSurf)  # number of control points
npatch = nb // 16  # number of patches

# one patch is 4 points
# B as defined in matlab

# % La matrice B stocke tous les points de controle de tous les patchs
# % B(:,:,:,k) sont tous les points de controle du patch k
# % La dimension de B(:,:,:,k) est 4 x 4 x 3, i.e., 16 points de controle
# % a 3 coordonnees (x,y,z)
#
# % B(:,:,1,k): x-coordonnes des points de controle du patch k comme matrice 4 x 4
# % B(:,:,2,k): y-coordonnes des points de controle du patch k comme matrice 4 x 4
# % B(:,:,3,k): z-coordonnes des points de controle du patch k comme matrice 4 x 4

B = np.zeros((4, 4, 3, npatch))
for k in range(npatch):
    for i in range(4):
        for j in range(4):
            B[i, j, 0, k] = bezierSurf[i * 4 + j + k * 16, 0]
            B[i, j, 1, k] = bezierSurf[i * 4 + j + k * 16, 1]
            B[i, j, 2, k] = bezierSurf[i * 4 + j + k * 16, 2]

num_p = 20;  # nombre de valeurs de parametre en direction u et v
num_n = 20;  # plus num_p est grand plus la surface paraitra lisse
# et plus le calcul sera long

# num_p+1 valeurs de parametres uniformes: 0,1,2,...,num_p en u et v
u = np.linspace(0, 1, num_p)
v = np.linspace(0, 1, num_n)

S = np.zeros((len(u), len(v), 3, npatch))
for k in range(npatch):
    S[:, :, :, k] = bezierPatchEval(B[:, :, :, k], u, v)



plotSurface(S, npatch)
plotControPoly(B, npatch)
