from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt
import numpy as np


def plotSurface(S, npatch):
    # all info is in S (there is x, y and z i think)
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    for k in range(npatch):
        X = S[:, :, 0, k]
        Y = S[:, :, 1, k]
        Z = S[:, :, 2, k]

        # to get the same plots as in the files from Matlab, decomment one of the following lines
        # surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, color='green', edgecolor='black',linewidth=1)
        # surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap='viridis', edgecolor='black',linewidth=1)
        surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap='viridis', edgecolor='none')

    plt.show()
    return


def plotControPoly(B, npatch):
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    for k in range(npatch):
        X = B[:, :, 0, k]
        Y = B[:, :, 1, k]
        Z = B[:, :, 2, k]
        surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1,
                               linewidth=0.5, color='yellow', shade=True, edgecolor='black')

    plt.show()
    return
