import numpy as np

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# % Evaluation d'un patch de Bezier aux parametres u,v
# %
# % Input:
# %  - matrix B de 16 points de controle de dim 3
# %     chaque point de controle a 3 coordonnes (x,y,z)
# %     taille de B: 4x4x3
# %     B(:,:,k) keme coordonnes des16  points de controle, k=1,2,3
# %     B(i,j,:) les 3 coordonnes du point de controle b_ij
# %  - u Vecteur de |u|=length(u) valeurs de parametre en u
# %  - v Vecteur de |v|=length(v) valeurs de parametre en v
#
# % Output:
# %  - matrix S avec la grille de |u|x|v| points 3D sur la surface
# %    La structure de S est similaire a celle de B.
# %    Taille de S:  len(u) * len(v) *3
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

M = np.array([[1, 0, 0, 0],
              [-3, 3, 0, 0],
              [3, -6, 3, 0],
              [-1, 3, -3, 1]])
MT = M.transpose()


def bezierPatchEval(B, u, v):
    S = np.zeros((len(u), len(v), 3))
    for i in range(len(u)):
        for j in range(len(v)):
            U = np.array([1, u[i], u[i] ** 2, u[i] ** 3])
            VT = np.array([1, v[j], v[j] ** 2, v[j] ** 3]).transpose()
            for k in range(3):
                res = U.dot(M.dot(B[:, :, k].dot(MT.dot(VT))))
                S[i, j, k] = res
                # print(i, j, k, S[i, j, k])
    return S

"""
calcule la normale via la formule de l'énoncé
le produit vectoriel est réalisé par une fonction numpyil faut connaitre les dérivées partielles X_u et X_v
retourne un champ de normale, donc une liste de vecteursn vecteur  = 3coordonnées
"""
def bezierPatchNormal(B, u, v):
