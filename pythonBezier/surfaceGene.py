"""

Code use to generation 3 other surface in ascii format
Surcafe are supposed to have 16 * k points of interest
"""
import numpy as np
from bezierPatchEval import bezierPatchEval
from plotBezierSurface3D import plotSurface, plotControPoly
import math as m

LISTE_COORD = [0, 1, 2, 3]




def cosinus(coord_x, coord_y):
    return m.cos(coord_y) + m.cos(coord_x)

def carre(coord_x, coord_y):
    return coord_x**2 + coord_y**2

def modification_simple(fonction):
    surface = loadControlPoints("surface_simple.txt")
    nb = len(surface)  # number of control points
    npatch = nb // 16  # number of patches
    for k in range(npatch):
        for i in range(4):
            for j in range(4):
                surface[i * 4 + j + k * 16, 2] = fonction(surface[i * 4 + j + k * 16, 0],
                 surface[i * 4 + j + k * 16, 1])
    return surface


def modification_complexe(fonction1, fonction2):
    surface = loadControlPoints("surface_double.txt")
    nb = len(surface)  # number of control points
    npatch = nb // 16  # number of patches
    k=0
    for i in range(4):
        for j in range(4):
            surface[i * 4 + j + k * 16, 2] = fonction1(surface[i * 4 + j + k * 16, 0],surface[i * 4 + j + k * 16, 1])
            surface[i * 4 + j + (k+1) * 16, 2] = fonction2(surface[i * 4 + j + (k+1) * 16, 0], surface[i * 4 + j + (k+1) * 16, 1])
    for i in range(4):
        surface[i * 4 + 3 + k * 16, 2] = surface[i * 4 + 0 + (k+1) * 16, 2]

    return surface

def loadControlPoints(filename):
    points = np.loadtxt(filename)
    return points

def print_surface(surface):
    nb = len(surface)  # number of control points
    npatch = nb // 16  # number of patches
    for k in range(npatch):
        for i in range(4):
            for j in range(4):
                print(str(surface[i * 4 + j + k * 16, 0]) + "  " +
                    str(surface[i * 4 + j + k * 16, 1]) + "  " +
                        str(surface[i * 4 + j + k * 16, 2]))


# print_surface(modification_simple(carre))
print_surface(modification_complexe(cosinus, carre))
